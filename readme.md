# Step Wizard plugin

## Overview
Reveals content one step at a time, e.g. useful with registration steps.


Place content you want to display inside div's with class ``.step[n]`` (n is desired step number).


Below is an example of steps only with option for next step.

#### Required HTML structure
```html
  <div class="step1">
    <h2>Step 1 content</h2>
    <div class="buttons">
      <button class="show_step" data-step="2">Next step</button>
    </div>
  </div>
  <div class="step2">
    <h2>Step 2 content</h2>
    <div class="buttons">
      <button class="show_step" data-step="3">Next step</button>
    </div>
  </div>
  <div class="step3">
    <h2>Step 3 content</h2>
    <div class="buttons">
      <button class="submit-button">Submit</button>
    </div>
  </div>


```
_If you want you can add steps navigation._

__Step navigation links must have classes and data atrributes like in the example below.__

```html
  <div class="steps-wrapper">
    <ul class="steps">
      <li class="selected">
        <span>Step 1:</span><a href="#" class="show_step show_step1" data-step="1">Link</a>
      </li>
      <li>
        <span>Step 2:</span><a href="#" class="show_step show_step2" data-step="2">Link</a>
      </li>
      <li>
        <span>Step 3:</span><a href="#" class="show_step show_step3" data-step="3">Link</a>
      </li>
      <li>
        <span>Step 4:</span><a href="#" class="show_step show_step4" data-step="4">Link</a>
      </li>
    </ul>
  </div>
```

Another option is to add Prev/Next buttons for navigation which are always visible.

__Here is the required HTML structure.__
```html
  <div class="step_wizard_control">
    <a href="#" class="prev">Previous</a>
    <a href="#" class="next">Next</a>
  </div>

```

## Usage:

### Required CSS and attributes
Initially hide all steps.
```css
.step1, .step2, .step3, .step4 {display: none;}
```
__This plugin reads data-step attribute from clicked element to know which step is clicked.__

Step wizard requires a few classes and data property for it to function properly:
 
  *  ``.show_step``       - elements which trigger step render must have this class
  *  ``.show_step[n]``    - required only on steps navigation links (``.show_step1``)
  *  ``data-step="[n]"``  - value of next or current step

> ** [n] is desired step number


### Via Javascript
Enable manually with:
```javascript
  $(window).on('load').step_wizard();
```

### Options via Javascript
```js
  $(window).on('load').step_wizard({
    max_steps: 4,
    current_step: 2
  });
```

### Options

| Name          |type           |default        | description                                                                                      |
| ------------- |:-------------:|:-------------:| -------------                                                                                    |
| current_step  | integer       | 1     | which step you want to display first                                                                                                                 |
| max_steps     | integer       | 0     | If you want to enable Prev/next button navigation then set this to max number of steps you have ``(don't forget to provide markup for navigation)``. |
| steps_nav     | boolean       | false | If you are using steps navigation links then set this to ``true``                                                                                    |


## DEMO instructions
###In order to be able to try demo you need to do the following: 
  + ``git clone`` this repository 
  + run ``bower install``
  + run ``http-server``