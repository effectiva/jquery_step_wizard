(function(){
  'use strict';

  var StepWizard = function(el, opts){
    this.opts = $.extend({

    }, opts || {});

    this.current_step = this.opts.current_step || 1;
    this.last_step = 1;
    this.step_selector = '.step';
    this.steps_wrapper = '.steps-wrapper';
    this.steps_nav = this.opts.steps_nav || false;
    this.max_steps = this.opts.max_steps || 0;
    this.setup_events();
    this.render_step(this.current_step);
  };

  StepWizard.prototype.setup_events = function() {
    var self = this;

    $('.show_step').click(function(e) {
      e.preventDefault();
      self.render_step(parseInt($(this).data('step'), 10));
    });

    if(this.max_steps === 0){ return; }

    $('.step_wizard_control .next').click(function(e){
        e.preventDefault();
        self.next();
    });

    $('.step_wizard_control .prev').click(function(e){
        e.preventDefault();
        self.prev();
    });

  };

  StepWizard.prototype.render_step = function(step) {
    step && (this.current_step = step);

    $(this.step_selector + this.last_step).hide();
    $(this.step_selector + this.current_step).show();

    if(this.steps_nav){ this.update_navigation(); }
    this.move_to_top();
    this.last_step = this.current_step;
  };

  StepWizard.prototype.update_navigation = function() {
    $(this.steps_wrapper +' .selected').removeClass('selected');
    $(this.steps_wrapper +' .show_step' + this.current_step).closest('li').addClass('selected');
  };

  StepWizard.prototype.move_to_top = function() {
    var self = this;
    $('html, body').animate({
      scrollTop: $(self.steps_wrapper).offset().top
    }, 400);
  };

  StepWizard.prototype.next = function() {
    this.current_step++;
    if(this.current_step > this.max_steps) { this.current_step = this.max_steps; }
    this.render_step();
  };

  StepWizard.prototype.prev = function() {
    this.current_step--;
    if(this.current_step < 1) { this.current_step = 1; }
    this.render_step();
  };


  //Expose as jquery plugin
  var plugin = 'step_wizard';
  var Klass = StepWizard;

  $.fn[plugin] = function(options){
      var method = typeof options === 'string' && options;
      $(this).each(function(){
          var $this = $(this);
          var instance = $this.data(plugin);
          if(instance){
            method && instance[method]();
            return;
          }
          instance = new Klass(this, options);
          $this.data(plugin, instance);
      });
      return this;
  };

  //Expose class
  $[Klass] = Klass;

})();
